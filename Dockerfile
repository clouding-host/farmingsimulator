FROM debian:stretch

ARG MONO_VER="5.1.1"
ARG GECKO_VER="2.47.1"

RUN dpkg --add-architecture i386 \
    && apt-get update \
    && apt-get install -y --no-install-recommends \
        curl \
        ca-certificates \
        gnupg \
        wget \
        apt-transport-https \
    && echo 'deb https://dl.winehq.org/wine-builds/debian/ stretch main' > /etc/apt/sources.list.d/wine.list \
    && curl -fsSL https://dl.winehq.org/wine-builds/winehq.key | apt-key add - \
    && apt-get update \
	&& apt-get install -y --no-install-recommends \
        haproxy \
        openssh-server \
        supervisor \
        winbind \
        winehq-stable \
        xvfb \
	&& mkdir -p /usr/share/wine/mono \
	&& mkdir -p /usr/share/wine/gecko \
    && wget https://dl.winehq.org/wine/wine-mono/${MONO_VER}/wine-mono-${MONO_VER}-x86.msi \
        -O /usr/share/wine/mono/wine-mono-${MONO_VER}-x86.msi \
    && wget https://dl.winehq.org/wine/wine-gecko/${GECKO_VER}/wine-gecko-${GECKO_VER}-x86.msi \
        -O /usr/share/wine/gecko/wine-gecko-${GECKO_VER}-x86.msi \
    && wget https://dl.winehq.org/wine/wine-gecko/${GECKO_VER}/wine-gecko-${GECKO_VER}-x86_64.msi \
        -O /usr/share/wine/gecko/wine-gecko-${GECKO_VER}-x86_64.msi \
    && /usr/bin/wine64 msiexec /i /usr/share/wine/mono/wine-mono-${MONO_VER}-x86.msi \
    && /usr/bin/wine64 msiexec /i /usr/share/wine/gecko/wine-gecko-${GECKO_VER}-x86.msi \
    && /usr/bin/wine64 msiexec /i /usr/share/wine/gecko/wine-gecko-${GECKO_VER}-x86_64.msi \
	&& wget  https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks \
    && chmod +x winetricks \
    && cp winetricks /usr/local/bin/ \
    && wget  https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks.bash-completion \
    && cp winetricks.bash-completion /usr/share/bash-completion/completions/winetricks \
    && adduser -D -h /home/container container \
    && /usr/bin/winecfg && /usr/local/bin/winetricks --unattended settings win10 \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get autoremove -y \
    && apt-get autoclean -y \

USER container
ENV WINEPREFIX=/home/container
ENV USER=container HOME=/home/container
WORKDIR /home/container
RUN winecfg \
    && winetricks --unattended settings win10

COPY ./entrypoint.sh /entrypoint.sh

CMD ["/bin/bash", "/entrypoint.sh"]